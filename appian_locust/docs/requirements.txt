locust==2.15.1
uritemplate==4.1.1
sphinx_rtd_theme==1.2.2
sphinx==6.2.1
Jinja2==3.0.3
